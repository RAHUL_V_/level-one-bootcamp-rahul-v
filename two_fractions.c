//WAP to find the sum of two fractions.
#include<stdio.h>

typedef struct fract
{
    int n;
    int d;
}FRAC;

FRAC input()
{
  FRAC f;
  printf("Enter numerator ");
  scanf("%d",&f.n);
  printf("Enter denominator ");
  scanf("%d",&f.d);
  return f;
}
 int calculation(FRAC f1,FRAC f2)
 {
     int x,y,g,gcd;
     if(f1.d==f2.d)
     {
         x=f1.n+f2.n;
         y=f1.d;
     }
     else
     {
         x=(f1.n*f2.d)+(f2.n*f1.d);
         y=(f1.d*f2.d);
     }
     if(x<=y)
         g=y;
     else
         g=x;
    for(int i=1;i<=g;i++)
    {
        if(x%i==0 && y%i==0)
          gcd=i;
     
    }
    printf("the addition of two fraction is %d / %d",x/gcd,y/gcd);
    return 0;
 }
 void main()
 {
     FRAC f1,f2;
     f1=input();
     f2=input();
     calculation(f1,f2);
    
 }