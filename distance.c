//WAP to find the distance between two point using 4 functions.
#include<stdio.h>
#include<math.h>

float point1x()
{
float x1;
printf("Enter the coordinate of x");
scanf("%f",&x1);
return x1;
}

float point1y()
{
float y1;
printf("Enter the coordinate of y");
scanf("%f",&y1);
return y1;
}

float point2x()
{
float x2;
printf("Enter the coordinate of x2");
scanf("%f",&x2);
return x2;
}

float point2y()
{
float y2;
printf("Enter the coordinate of y2");
scanf("%f",&y2);
return y2;
}
float distance(float x1,float y1,float x2,float y2)
{
float d;
d = sqrt((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1));
printf("the distance between %f,%f and %f, %f is %f",x1,y1,x2,y2,d);
}
float main()
{
float x1 ,y1,x2,y2,d;
x1=point1x();
y1=point1y();
x2=point2x();
y2=point2y();
distance(x1,y1,x2,y2);
return 0;
}
