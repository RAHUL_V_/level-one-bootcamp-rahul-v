//Write a program to add two user input numbers using 4 functions.
#include<stdio.h>
int input1()
{
int a;
printf("Enter the number ");
scanf("%d",&a);
return a;
}
int input2()
{
int b;
printf("Enter the number ");
scanf("%d",&b);
return b;
}
int sum(int a,int b)
{
int s ;
s = a+b;
return s;
}
int out(int s)
{
printf("the sum of two numbers is %d",s);
return 0;
}
int main()
{
int a,b,s;
a=input1();
b=input2();
s=sum(a,b);
out(s);
}
